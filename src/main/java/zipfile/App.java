/*Задача 1.
Написать простое консольное приложение для zip архивации файлов.

1. Программа должна запросить путь входного файла и его валидность.
(В случае неправильного пути запросить путь повторно или завершить работу программы.)
2. Программа должна запросить ввести имя архива
3. Программа должна запросить выбрать уровень компрессии файла.
4. После окончания работы программа должна вывести информацию о архивируемом файле и файле архива.
А также сообщение об успешном окончании работы приложения. */

package zipfile;

import zipfile.logic.MyZipFile;

import java.io.*;

public class App {
    public static void main(String[] args) {

        BufferedReader reader = null;
        String sSourceFile = null;
        String sZipFile = null;
        int iCompLevel = 0;
        boolean isNumberCorrect = false;
        boolean isFileCorrect = false;

        try{
            reader = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Enter file you want to zip('q' to exit): ");

            while (!isFileCorrect){
                sSourceFile = reader.readLine();
                File file = new File(sSourceFile);

                if(sSourceFile.equalsIgnoreCase("q"))
                    return;

                if(file.exists())
                    isFileCorrect = true;
                else
                    System.out.println(file.getName() + " is not found, try again:");
            }

            System.out.println("Enter zipfile name('q' to exit):");
            sZipFile = reader.readLine();

            if(sZipFile.equalsIgnoreCase("q"))
                return;

            while (!isNumberCorrect) {
                System.out.println("Enter compression level(0-9) ('q' to exit):");
                String str = reader.readLine();

                if(str.equalsIgnoreCase("q"))
                    return;

                try {
                    iCompLevel = Integer.parseInt(str);

                    if (iCompLevel >= 0 && iCompLevel < 10)
                        isNumberCorrect = true;
                    else
                        System.out.println("incorrect level number, try again: ");
                } catch (NumberFormatException nfe) {
                    System.out.println(nfe.getMessage() + " is not number");
                }
            }

            MyZipFile myZipFile = new MyZipFile(sSourceFile, sZipFile, iCompLevel);
            myZipFile.makeZip();
            myZipFile.printInfo();

            System.out.println("Congratulations! Your file " + sZipFile + " zipped & ready to use!");

        } catch (FileNotFoundException fnfe){
            System.out.println("File " + sSourceFile + " is not found " + fnfe);
        } catch (IOException ioe){
            ioe.printStackTrace();
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

}