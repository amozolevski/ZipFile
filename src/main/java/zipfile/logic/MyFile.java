package zipfile.logic;

import java.io.File;

public abstract class MyFile extends File {

    public MyFile(String pathname) {
        super(pathname);
    }

    public abstract void printInfo();
}