package zipfile.logic;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class MyZipFile extends MyFile implements ZipMaker{

    private String sSourceFile;
    private String sZipFile;
    private int iCompLevel;

    public MyZipFile(String sSourceFile, String sZipFile, int iCompLevel) {
        super(sSourceFile);
        this.sSourceFile = sSourceFile;
        this.sZipFile = sZipFile;
        this.iCompLevel = iCompLevel;
    }

    public void makeZip(){

        ZipOutputStream zipOut = null;
        FileInputStream fileInput = null;

        try {
            zipOut = new ZipOutputStream(new FileOutputStream(sZipFile));
            zipOut.setLevel(iCompLevel);

            fileInput = new FileInputStream(sSourceFile);

            ZipEntry entry = new ZipEntry(sSourceFile);
            zipOut.putNextEntry(entry);

            byte[] bytes = new byte[1024];

            int length;
            while ((length = fileInput.read(bytes)) >= 0){
                zipOut.write(bytes, 0, length);
            }

        } catch (FileNotFoundException fnfe){
            fnfe.getStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        } finally {
            try{
                if(zipOut != null)
                    zipOut.close();
                if(fileInput != null)
                    fileInput.close();
            } catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    public void printInfo() {
        File file = new File(sZipFile);
        System.out.println("---File name: " + this.getName() + "---");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        System.out.println("last modified: " + simpleDateFormat.format(this.lastModified()));

        System.out.println("file length: " + this.length() + " byte(s)");
        System.out.println("------------------------------");

        System.out.println();

        System.out.println("---File name: " + file.getName() + "---");

        System.out.println("last modified: " + simpleDateFormat.format(file.lastModified()));

        System.out.println("file length: " + file.length() + " byte(s)");
        System.out.println("------------------------------");
    }
}